#ifndef ARCHIVER_HPP
#define ARCHIVER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <chrono>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Infos.hpp>

using std::string;

namespace archiver{
	struct Config{
		string basePath, logPath, useragent, password;
		long downloadTimeout;
		unsigned short port;
	};

	struct ConfigReaderFail{
		string errorMsg;
		unsigned long long lineNumber;
		ConfigReaderFail(const string newErrorMsg, const unsigned long long newLineNumber){
			errorMsg=newErrorMsg; lineNumber=newLineNumber;
		}
	};

	enum ConfigReaderState{
		statePort,
		stateBasePath,
		stateLogPath,
		stateDownloadTimeout,
		stateUseragent,
		statePassword,
		stateFinished
	};

	string get_date(){
		const std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		const string timeStr = std::ctime(&time);
		return timeStr.substr(0, timeStr.size()-1); // Remove trailing newline
	}

	bool stream_is_empty(std::stringstream &stream){
		stream.seekg(0, std::ios::beg);
		return stream.eof();
	}

	bool file_exists(const string filename){
		std::ifstream file(filename);
		return file.good();
	}

	Config read_config(const string filename){

		std::ifstream configFile(filename);

		if (!configFile){
			ConfigReaderFail f("Failed to open file", 0);
			throw f;
		}

		Config out;
		ConfigReaderState state = statePort;
		string line;
		for (unsigned long long lineNumber = 0; std::getline(configFile, line); ++lineNumber){
			if (line.empty()) continue;
			if ((line[0] == '#')) continue;

			if (state == statePort){
				try{
					out.port = std::stoi(line);
				} catch(std::invalid_argument &){
					ConfigReaderFail f("Failed to convert string to int", lineNumber);
					throw f;
				}
			} else if (state == stateBasePath){
				out.basePath = line;

			} else if (state == stateLogPath){
				out.logPath = line;

			} else if (state == stateDownloadTimeout){
				try{
					out.downloadTimeout = std::stol(line);
				} catch(std::invalid_argument &){
					ConfigReaderFail f("Failed to convert string to long", lineNumber);
					throw f;
				}

			} else if (state == stateUseragent){
				out.useragent = line;
			} else if (state == statePassword){
				out.password = line;
			}

			state = ConfigReaderState((state+1)%6);
		}

		return out;
	}

	bool archive_one_file(const string url, const string filepath, Config config, const string description="EMPTY"){
		std::ofstream archiveLogFile;

		// Make sure the CSV log header exists
		if (!file_exists(config.logPath)){
			archiveLogFile.open(config.logPath, std::fstream::out | std::fstream::app);
			archiveLogFile << "date,filepath,description,url,httpcode,success\n";
		} else{
			archiveLogFile.open(config.logPath, std::fstream::out | std::fstream::app);
		}

		std::stringstream downloadStream;

		curlpp::Easy client;

		client.setOpt(new curlpp::options::Url(url));
		client.setOpt(new curlpp::options::WriteStream(&downloadStream));

		client.setOpt(new curlpp::options::Timeout(config.downloadTimeout));
		client.setOpt(new curlpp::options::UserAgent(config.useragent));

		bool curlppError = false;

		try{
			client.perform();
		} catch(curlpp::LibcurlRuntimeError &e){
			curlppError = true;
		}

		bool success = !stream_is_empty(downloadStream);
		long long httpCode = -1;
		if (curlppError)
			success = false;
		else
			httpCode = curlpp::infos::ResponseCode::get(client);

		if (!success)
			return false;

		std::fstream outputFile(filepath, std::ofstream::out);
		if (!outputFile.good())
			return false;
		outputFile << downloadStream.rdbuf();

		archiveLogFile << get_date() << ',' << filepath << ',' << description << ',' << url << ',' << httpCode << ',' << success << '\n';

		return true;
	}
}

#endif // ARCHIVER_HPP
