#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <filesystem>
#include <SFML/Network.hpp>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>

#include "archiver.hpp"
#include "someones_urlparser/EdUrlParser.h"

using std::string;
using std::vector;

int main(){
	archiver::Config config;
	try{
		config = archiver::read_config("config");
	} catch(archiver::ConfigReaderFail &){
		std::cout << "Failed reading config\n";
	}

	sf::TcpListener listener;
	listener.listen(config.port);

	bool quit=false;
	while (!quit){
		sf::TcpSocket client;
		if (listener.accept(client) == sf::Socket::Done){
			char buffer[8192];
			std::size_t received = 0;
			client.receive(buffer, sizeof(buffer), received);
			string urlToParse = string(buffer).substr(4, string(buffer).substr().find(" ",4)-4);

			if (urlToParse.find("favicon.ico") != string::npos)
				continue;

			EdUrlParser* url = EdUrlParser::parseUrl(urlToParse);

			string urlToArchive, filename, description="EMPTY", password;

			vector <query_kv_t> kvs;
			unsigned num = EdUrlParser::parseKeyValueList(&kvs, url->query);
			for (unsigned i=0; i<num; i++){
				if (kvs[i].key == "link") urlToArchive = curlpp::unescape(kvs[i].val);
				else if (kvs[i].key == "filename") filename = kvs[i].val;
				else if (kvs[i].key == "description") description = kvs[i].val;
				else if (kvs[i].key == "password") password = kvs[i].val;
			}

			std::cout << password << '\n';

			if (password != config.password)
				continue;

			// Make sure the basePath exists
			if (!std::filesystem::exists(config.basePath))
				std::filesystem::create_directory(config.basePath);

			archiver::archive_one_file(urlToArchive, config.basePath + '/' + filename, config, description);

			string msg = client.getRemoteAddress().toString();
			client.send(msg.c_str(), msg.size());
		}
	}
}
